const Alexa = require('ask-sdk-core');
const http = require('http');

function consumePokemonApi(uri) {
  return new Promise(((resolve, reject) => {
    var options = {
        host: '195.83.128.21',
        port: 80,
        path: uri,
        method: 'GET',
    };
   
    const request = http.request(options, (response) => {
      response.setEncoding('utf8');
      let returnData = '';
      console.log(response);

      response.on('data', (chunk) => {
        returnData += chunk;
      });

      response.on('end', () => {
        resolve(returnData);
      });

      response.on('error', (error) => {
        reject(error);
      });
    });
    request.end();
  }));
}

const LaunchRequestHandler = {
  canHandle(handlerInput) {
    return handlerInput.requestEnvelope.request.type === 'LaunchRequest';
  },
  handle(handlerInput) {
    const speechText = 'Bienvenue dans votre pokedex, demandez moi ce que vous voulez !';

    return handlerInput.responseBuilder
      .speak(speechText)
      .reprompt(speechText)
      .withSimpleCard('Hello World', speechText)
      .getResponse();
  }
};

const CancelAndStopIntentHandler = {
  canHandle(handlerInput) {
    return handlerInput.requestEnvelope.request.type === 'IntentRequest'
      && (handlerInput.requestEnvelope.request.intent.name === 'AMAZON.CancelIntent'
        || handlerInput.requestEnvelope.request.intent.name === 'AMAZON.StopIntent');
  },
  handle(handlerInput) {
    const speechText = 'Goodbye!';

    return handlerInput.responseBuilder
      .speak(speechText)
      .withSimpleCard('Hello World', speechText)
      .withShouldEndSession(true)
      .getResponse();
  }
};

const SessionEndedRequestHandler = {
  canHandle(handlerInput) {
    return handlerInput.requestEnvelope.request.type === 'SessionEndedRequest';
  },
  handle(handlerInput) {
    //any cleanup logic goes here
    return handlerInput.responseBuilder.getResponse();
  }
};

const ErrorHandler = {
  canHandle() {
    return true;
  },
  handle(handlerInput, error) {
    console.log(`Error handled: ${error.message}`);

    return handlerInput.responseBuilder
      .speak('J\'ai rien compris, reviens me voir quand tu parleras correctement.')
      .reprompt('J\'ai rien compris, reviens me voir quand tu parleras correctement.')
      .getResponse();
  },
};

const PokemonIntentHandler = {
  canHandle(handlerInput) {
    return handlerInput.requestEnvelope.request.type === 'IntentRequest'
      && handlerInput.requestEnvelope.request.intent.name === 'PokemonIntent';
  },
  handle(handlerInput) {
    const speechText = 'J\'adore Carapuce';

    return handlerInput.responseBuilder
      .speak(speechText)
      .withShouldEndSession(false)
      .getResponse();
  }
};

const GetPokemonTypeIntentHandler = {
  canHandle(handlerInput) {
    return handlerInput.requestEnvelope.request.type === 'IntentRequest'
      && handlerInput.requestEnvelope.request.intent.name === 'GetPokemonTypeIntent';
  },
  async handle(handlerInput) {
      var speechText;
    if (handlerInput.requestEnvelope.request.intent.slots.pokemon === undefined) {
        speechText = 'Je ne possède pas ce pokémon dans ma base de données...';
        
        return handlerInput.responseBuilder
          .speak(speechText)
          .withShouldEndSession(false)
          .getResponse();
    }
    const name = await consumePokemonApi('http://195.83.128.21/~dev1919/pokedex/getPokemonType.php?id='+handlerInput.requestEnvelope.request.intent.slots.pokemon.resolutions.resolutionsPerAuthority[0].values[0].value.id);
    speechText = handlerInput.requestEnvelope.request.intent.slots.pokemon.value + ' est un pokémon de type ' + name ;

    return handlerInput.responseBuilder
      .speak(speechText)
      .withShouldEndSession(false)
      .getResponse();
  }
};

const GetPokemonByIdIntentHandler = {
  canHandle(handlerInput) {
    return handlerInput.requestEnvelope.request.type === 'IntentRequest'
      && handlerInput.requestEnvelope.request.intent.name === 'getPokemonByIdIntent';
  },
  async handle(handlerInput) {
      var speechText;
   
    const name = await consumePokemonApi('http://195.83.128.21/~dev1919/pokedex/getPokemon.php?id='+handlerInput.requestEnvelope.request.intent.slots.pokemonid.value);
    speechText = 'le pokemon numéro ' + handlerInput.requestEnvelope.request.intent.slots.pokemonid.value + 's\'appelle  ' + name;

    return handlerInput.responseBuilder
      .speak(speechText)
      .withShouldEndSession(false)
      .getResponse();
  }
};

const RandomPokemonIntentHandler = {
  canHandle(handlerInput) {
    return handlerInput.requestEnvelope.request.type === 'IntentRequest'
      && handlerInput.requestEnvelope.request.intent.name === 'RandomPokemonIntent';
  },
  async handle(handlerInput) {
    const speechText = await consumePokemonApi('http://195.83.128.21/~dev1919/pokedex/getPokemon.php');

    return handlerInput.responseBuilder
      .speak(speechText)
      .withShouldEndSession(false)
      .getResponse();
  }
};

exports.handler = Alexa.SkillBuilders.custom()
  .addRequestHandlers(
    LaunchRequestHandler,
    CancelAndStopIntentHandler,
    SessionEndedRequestHandler,
    PokemonIntentHandler,
    GetPokemonTypeIntentHandler,
    GetPokemonByIdIntentHandler,
    RandomPokemonIntentHandler)
  .addErrorHandlers(ErrorHandler)
  .lambda();