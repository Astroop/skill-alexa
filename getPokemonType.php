<?php

$pokemon =  json_decode(file_get_contents('https://pokeapi.co/api/v2/pokemon/'.$_GET['id'].'/'));

$types = $pokemon->types;
$pokemonTypes = "";
foreach($types as $type) {
    $pokemonTypes .= $type->type->name.', ';    
}

echo ucfirst($pokemonTypes);