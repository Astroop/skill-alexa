<?php
$max = 805;

if(isset($_GET['id'])) {
    $id = $_GET['id'];
} else {
    $id = random_int(1, $max);
}

$pokemon =  json_decode(file_get_contents('https://pokeapi.co/api/v2/pokemon-species/'.$id.'/'));

$pokemonName = $pokemon->name;
$names = $pokemon->names;

foreach($names as $name) {
    if($name->language->name == 'fr') {
        $pokemonName = $name->name;
        break;
    } 
}
echo ucfirst($pokemonName);